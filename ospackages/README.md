* * *
# os package definition files
* * *
##  Purpose

* Definition files contained in this directory
* one file per supported Linux os
* Each file contains the os packages required for installing python3.

* * *

##  Parse Syntax

```

#!/usr/bin/env bash

declare -a temparray

while read -r line; do
    temparray=( "${temparray[@]}" "$(echo $line | awk -F ':' '{print $1}')"  )
done < 'AmazonLinux2.txt'

```
