#!/usr/bin/env bash

declare -a container

while read -r line; do
    container=( "${container[@]}"  $(echo $line | awk -F ':' '{print $1}') )
done < 'AmazonLinux2.txt'


for i in "${container[@]}"; do
    echo $i
done
