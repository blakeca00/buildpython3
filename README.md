* * *
# Build Python3 Linux
* * *

## About this repository

* **Purpose**: 		Compile & Build Python3 on Linux
* **Version**:	1.8.12
* **Docs**:         https://buildpy.readthedocs.io

* * *
## Contents
* * *

* [**SUMMARY**](#markdown-header-summary)

* [**GETTING STARTED**](#markdown-header-getting-started)

* [**SUPPORTED LINUX DISTRIBUTIONS**](#markdown-header-supported-linux-distributions)

* [**DEPENDENCIES**](#markdown-header-dependencies)

* [**INSTALLATION**](#markdown-header-installation)
    * [Ubuntu, Linux Mint, Debian Variants](#markdown-header-installation)
    * [Redhat, CentOS, Fedora RPM-based Distributions](#markdown-header-redhat-centos-fedora-rpm-based-distributions)

* [**UPGRADING**](#markdown-header-upgrading)
    * [Ubuntu, Linux Mint, Debian Variants](#markdown-header-upgrading)
    * [Redhat-based Distributions](#markdown-header-upgrading-redhat-based-distributions)

* [**UNINSTALL**](#markdown-header-uninstall)

* [**USAGE**](#markdown-header-usage)
    * [Verify OS Dependencies](#markdown-header-usage)
    * [Compile and install Python 3](#markdown-header-usage-compile-and-install-python)
    * [Unattended Use](#markdown-header-usage-unattended-use)

* [**HELP**](#markdown-header-help)

* [**SCREENSHOTS**](#markdown-header-screenshots)
    * [OS Detection](#markdown-header-screenshots-operating-system-detection)
    * [Show Specific Python Version Status](#markdown-header-screenshots-show-python-version-status)
    * [Show OS Package Prerequisites](#markdown-header-screenshots-show-os-packages)
    * [Download Python Source (DEFAULT)](#markdown-header-screenshots-download-python-source-default)
    * [Download Python Source (Named Version)](#markdown-header-screenshots-download-python-source-named-version)
    * [Remve Build Artifacts](#markdown-header-screenshots-remove-build-artifacts)
    * [Package Information](#markdown-header-screenshots-package-information)
    * [Log File](#markdown-header-screenshots-log-file)

* [**AUTHOR & COPYRIGHT**](#markdown-header-author-copyright)

* [**LICENSE**](#markdown-header-license)

* [**DISCLAIMER**](#markdown-header-disclaimer)

--

[back to the top](#markdown-header-build-python3-linux)

* * *
## Summary
* * *

**buildpy** features:

* Utility for build and installing python3 from source binaries on Linux.
* Compile Python 3.0 to 3.7+ major versions.  (Python 2.7 can be compiled and installed; however, not tested)
* Automatically find and install the latest minor version available for download from [https://www.python.org/ftp/python](https://www.python.org/ftp/python)
* Automated install via `quiet` mode for scripted configuration management installs

**NOTE**

* This utility builds Python binaries for the entire system.
* If you wish to build Python for a single user, consider [pipenv](https://github.com/pyenv/pyenv)
* **Feb 11, 2019**:  Debian Users, please download and install the new public key. To do so, just repeat **Step 1**, [Installation: Ubuntu, Linux Mint, Debian Variants](#markdown-header-installation))

[back to the top](#markdown-header-build-python3-linux)

* * *
## Getting Started

See the following resources for detailed information about **buildpy**:

* Read this [README](./README.md) document.

* See the [buildpy feature overview](https://docs.google.com/presentation/d/e/2PACX-1vQ3nLpP8xyMacrA7IeMU_Mo_dGN2u8JjM_hjIEdNe6wapRNVifdavPZW-3kDXki_SL_glFHZW9TzG_l/embed?start=false&loop=false&delayms=3000) to learn about **buildpy**.

[![](./assets/gdocs.png)](https://docs.google.com/presentation/d/e/2PACX-1vQ3nLpP8xyMacrA7IeMU_Mo_dGN2u8JjM_hjIEdNe6wapRNVifdavPZW-3kDXki_SL_glFHZW9TzG_l/embed?start=false&loop=false&delayms=3000)



[back to the top](#markdown-header-build-python3-linux)

* * *
## Supported Linux Distributions
* * *

* [Ubuntu 14.04](http://releases.ubuntu.com/14.04): Ubuntu variants based on 14.04+
* [Ubuntu 16.04](http://releases.ubuntu.com/16.04): Ubuntu variants based on 16.04, 16.10+
* [Ubuntu 18.04](http://releases.ubuntu.com/18.04): Ubuntu variants based on 18.04, 18.10+
* [Linux Mint 17](https://linuxmint.com/edition.php?id=158), [18](https://www.linuxmint.com/release.php?id=31), [19](https://www.linuxmint.com/download.php)
* [Redhat  7.3+, 8](https://access.redhat.com/products/red-hat-enterprise-linux)
* [Fedora 26+](https://getfedora.org)
* [Centos  7, 8](https://www.centos.org)
* [Amazon Linux 1](https://aws.amazon.com/amazon-linux-ami) (2017+)
* [Amazon Linux 2](https://aws.amazon.com/amazon-linux-2) (2018+)

**Note**
* Older versions than listed above may be compatible, but not have not been tested

[back to the top](#markdown-header-build-python3-linux)

* * *
## Dependencies
* * *

* _Root Privileges_.  Ability to assume user with root privileges or access such privileges via **sudo**
* **Python3-support via Operating System Packages**. Dependencies on various operating system packages
available from official Linux distribution's repository.  You can view os package dependencies with the
following command once **buildpy** is installed:

```
$ buildpy --show os-packages
```

* **Bash 4.1 or higher**.  No preexisting python implementation is required.  **buildpy** is written in [bash](https://www.gnu.org/software/bash/).


[back to the top](#markdown-header-build-python3-linux)

* * *
## Installation
* * *

### Debian, Ubuntu, Linux Mint, Ubuntu Variants

**Method 1**: Install the **debian-tools** repository.

The easiest way to install **buildpy** is via the Debian-tools repository:

1. Download the public key:

    ```
    $ wget -qO - http://awscloud.center/keys/public.key | sudo apt-key add -
    ```

2. Install the repository:

    ```
    $ sudo echo "deb [arch=amd64] http://deb.awscloud.center <distribution> main" > /etc/apt/sources.list.d/debian-tools.list
    ```

    **Where:** `<distribution>` is one of the following:

    * `trusty`:  Ubuntu 14.04, Ubuntu 14.04 based Linux distributions
    * `xenial`:  Ubuntu 16.04, 16.04 based Linux distributions
    * `bionic`:  Ubuntu 18.04, 18.04 based Linux distributions

3. Verify package repository installation

    ```
    $ apt list buildpy -a
    ```

    ![repository-contents](./assets/repo-contents.png)

4. Update and install the package:

    ```
    $ sudo apt update  &&  sudo apt install buildpy
    ```

5. Verify Installation.  To verify a Debian (.deb) package installation:

    ```
    $ apt show buildpy
    ```

    ![apt](./assets/apt-show.png)


[back to the top](#markdown-header-build-python3-linux)

* * *

**Method 2**: Install directly via `.deb` package.

If you do not want to install an additional repository on your local machine, you may instead download
and install the .deb package using the `apt` or `apt-get` package mangers.

1. **Download**:  the .deb installation package (v1.6.3 shown):

    ```
    $ wget https://bitbucket.org/blakeca00/buildpython3/downloads/buildpy-1.6.3_amd64.deb
    ```

2. **Install** via apt package manager:

    ```
    $ sudo apt install ./buildpy-1.6.3_amd64.deb
    ```

**Note**:

* Installation of the .deb package will also install the **debian-tools** repository on your local machine.
If you do not want this, please comment out the single line in `/etc/apt/sources.list.d/debian-tools.list`.

[back to the top](#markdown-header-build-python3-linux)

* * *

### Redhat, CentOS, Fedora RPM-based Distributions

The easiest way to install **buildpy** on redhat-based Linux distributions is via the developer-tools package repository:

(**Fedora** Users: use ```dnf``` in place of ```yum``` below)

1. Download and install the repo definition file

    ```
    $ sudo yum install wget
    ```

    ```
    $ wget https://bitbucket.org/blakeca00/buildpython3/downloads/developer-tools.repo
    ```

    ```
    $ sudo mv developer-tools.repo /etc/yum.repos.d/  &&  sudo chown 0:0 developer-tools.repo
    ```

2. Update local repository cache

    ```
    $ sudo yum update -y
    ```

3. Install **buildpy** os package

    ```
    $ sudo yum install buildpy
    ```

    ![rpm install](./assets/rpm-install-1.png)

    Answer "y":

    ![rpm install](./assets/rpm-install-2.png)


4. Verify Installation

    ```
    $ yum info buildpy
    ```

    ![verify-rpm](./assets/verify-rpm-install.png)


[back to the top](#markdown-header-build-python3-linux)

* * *
## Upgrading
* * *

To discover and apply newly-released versions of **buildpy**, follow the steps below for your specific distribution.

### Debian, Ubuntu, Linux Mint, Ubuntu Variants

**(1)** Find out if an upgraded version of **buildpy** is available:

```
$ sudo apt update && sudo apt list --upgradeable
```

![repository-contents](./assets/apt-upgrade-available.png)

Alternate:

```
$ apt list buildpy -a
```

![repository-contents](./assets/repo-contents-upgradeable.png)

**(2)** Install available upgrades:

```
$ sudo apt upgrade
```

**(3)**  Verify upgrade:

```
$ sudo apt list buildpy -a
```

![repository-contents](./assets/repo-contents-installed.png)

[back to the top](#markdown-header-build-python3-linux)

* * *

### Upgrading Redhat-based Distributions

**(1)** Find out if an upgraded version of **buildpy** is available

```
$ sudo yum info buildpy
```

![repository-contents](./assets/repo-yum-info.png)

**(2)** Install available upgrades:

```
$ sudo yum update -y
```

![repository-contents](./assets/repo-yum-update.png)

[back to the top](#markdown-header-build-python3-linux)

* * *
## Uninstall
* * *

### Debian, Ubuntu, Linux Mint, Ubuntu Variants

**buildpy** may be removed from your system using the debian package manager:

```
$ sudo apt remove buildpy
```

* * *

### Redhat, CentOS, Fedora

**buildpy** may be removed from your system using the yum package manager:

```
$ sudo yum erase buildpy
```


![rpm uninstall1](./assets/rpm-remove-1.png)

Answer "y":

![rpm uninstall2](./assets/rpm-remove-2.png)


[back to the top](#markdown-header-build-python3-linux)


* * *
## Usage
* * *

After you have [installed builpy](#markdown-header-installation), see below for common use cases.

**IMPORTANT**:

* Root privileges via sudo _required_; otherwise execute directly as root user

### [Usage](#markdown-header-usage) / Verify Operating System Dependencies

```
$ buildpy --show os-packages
```

![os-packages](./assets/show-os-packages.png)


[back to the top](#markdown-header-build-python3-linux)

* * *

### [Usage](#markdown-header-usage) / Compile and install Python

To compile and install the latest Python-3.6 binaries (3.6.7 at the time of this post), use the following command:

```
$ sudo buildpy --install Python-3.6
```

Or as root directly:

```
$ sudo su -
```

```
root@dev:~# buildpy --install 3.6
```

**NOTE**: You may also select an exact Python binary set to compile and install via the following syntax:

```
$ sudo buildpy --install Python-3.6.5
```

The `Python-` preceeding the version number may be omitted if desired:

```
$ sudo buildpy --install 3.6.5
```


[back to the top](#markdown-header-build-python3-linux)

* * *

### [Usage](#markdown-header-usage) / Unattended Use

If run via unattended script, use `--quiet` to suppress stdout messages:

```
$ sudo buildpy --install 3.6 --quiet
```

All console output if written to `/var/log/console.log` when running in unattended, quiet mode.

The running summary log is created in all execution states to `/var/log/buildpy.log`.  See an [example log file](#markdown-header-screenshots-log-file).



[back to the top](#markdown-header-build-python3-linux)

* * *
## Help
* * *

To display the help menu:

```
$ buildpy --help
```

![help](./assets/help-menu.png)


[back to the top](#markdown-header-build-python3-linux)

* * *
## Screenshots
* * *

### [Screenshots](#markdown-header-screenshots) / Operating System Detection

Test OS detection

```
$ buildpy  --os-detect
```

**Ubuntu** / **Linux Mint**

![os-detect](./assets/os-detect.png)

**Amazon Linux**

![os-detect](./assets/os-detect-aml.png)

[back to the top](#markdown-header-build-python3-linux)

* * *

### [Screenshots](#markdown-header-screenshots) / Show / Python Version Status

Show the latest Python 3.7 version available for install

```
$ buildpy  --show  Python-3.7
```

![py3.7](./assets/show3.7.png)

[back to the top](#markdown-header-build-python3-linux)

* * *
### [Screenshots](#markdown-header-screenshots) / Show / os-packages

Show operation system package prerequisites and installed status of each

```
$ buildpy  --show  os-packages
```

![os-packages](./assets/show-os-packages.png)

[back to the top](#markdown-header-build-python3-linux)

* * *

### [Screenshots](#markdown-header-screenshots) / Download Python Source (Default)

```
$ buildpy  --download        # default, download Python 3.6 binaries
```

![download](./assets/download.png)

[back to the top](#markdown-header-build-python3-linux)

* * *

### [Screenshots](#markdown-header-screenshots) / Download Python Source, Named Version

```
$ buildpy  --download  3.4
```

![download](./assets/download3.4.png)

[back to the top](#markdown-header-build-python3-linux)

* * *

### [Screenshots](#markdown-header-screenshots) / Remove Build Artifacts

```
$ buildpy --clean
```

![clean](./assets/clean.png)


[back to the top](#markdown-header-build-python3-linux)

* * *

### [Screenshots](#markdown-header-screenshots) / Package Information

Detailed information regarding the local installation of **buildpy** program and dependencies:

```
$ buildpy --info
```

![clean](./assets/pkg-info.png)


[back to the top](#markdown-header-build-python3-linux)

* * *

### [Screenshots](#markdown-header-screenshots) / Log file

**buildpy** actively maintains 2 log files:

* **buildpy.log**: Main system log messages (shown below). Located at `/var/log/buildpy.log`.

* **console.log**: During compile and installation of Python, stdout messages
are written to ```/var/log/console.log``` in addition to terminal console.  If installation is
executed with the `--quiet` flag set, there is no terminal console output and stdout is
written only to the console log file.

```

$ tail -n 100 /var/log/buildpy.log

```

![logfile](./assets/log-sample-default.png)


[back to the top](#markdown-header-build-python3-linux)

* * *
## Author & Copyright
* * *

All works contained herein copyrighted via below author unless work is explicitly noted by an alternate author.

* Copyright Blake Huber, All Rights Reserved.

[back to the top](#markdown-header-build-python3-linux)

* * *
## License
* * *

* Software contained in this repository is licensed under the [General Public License, v3](https://www.gnu.org/licenses/gpl-3.0.en.html).  You can review the full text on the web at the link or by reviewing the [license agreement document](./LICENSE.md) contained in this repository.

[back to the top](#markdown-header-build-python3-linux)

* * *
## Disclaimer
* * *

*Code is provided "as is". No liability is assumed by either the code's originating author nor this repo's owner for their use at AWS or any other facility. Furthermore, running function code at AWS may incur monetary charges; in some cases, charges may be substantial. Charges are the sole responsibility of the account holder executing code obtained from this library.*

Additional terms may be found in the complete [license agreement](./LICENSE.md).

[back to the top](#markdown-header-build-python3-linux)

* * *
