

Summary
----------------

-   `buildpy <https://buildpy.readthedocs.io>`__ is a utility for compiling and installing |link-sum1| on Linux.
-   Automatically downloads and compiles Python 2.7 to 3.7+ from `python.org <https://www.python.org/ftp/python>`__ source.
-   Manages the installation of Linux OS dependencies to enable python3 advanced runtime features.
-   Optional ``quiet`` mode for fully   automated, unattended installs.

.. admonition:: System-level Python3

    -   `buildpy <https://buildpy.readthedocs.io>`__ compiles, installs, and configures Python binaries for the entire system.
    -   If you wish to build Python for a single user, consider `pipenv <https://github.com/pyenv/pyenv>`__


.. |link-sum1| raw:: html

   <a href="https://www.python.org/downloads/source" target="_blank">python3 source binaries</a>


Back to `Table Of Contents <./index.html>`__

--------------

Getting Started
----------------

Before starting, we recommended reviewing the following:

-  Read the `Frequently Asked Questions <./FAQ.html>`__.

-  Reviewing the `buildpy feature
   overview <https://docs.google.com/presentation/d/e/2PACX-1vQ3nLpP8xyMacrA7IeMU_Mo_dGN2u8JjM_hjIEdNe6wapRNVifdavPZW-3kDXki_SL_glFHZW9TzG_l/embed?start=false&loop=false&delayms=3000>`__
   (below) to learn about  `buildpy <https://buildpy.readthedocs.io>`__.


.. image:: ../assets/gdocs.png
   :target: https://docs.google.com/presentation/d/e/2PACX-1vQ3nLpP8xyMacrA7IeMU_Mo_dGN2u8JjM_hjIEdNe6wapRNVifdavPZW-3kDXki_SL_glFHZW9TzG_l/embed?start=false&loop=false&delayms=3000


Back to `Table Of Contents <./index.html>`__

--------------

.. _Docs:

Documentation
-------------

**Online**

    -   Complete html documentation available at `http://buildpy.readthedocs.io <http://buildpy.readthedocs.io>`__.

**Download**:  Available via download in the formats below

    -   `pdf format <https://readthedocs.org/projects/buildpy/downloads/pdf/latest/>`__
    -   `Amazon Kindle <https://readthedocs.org/projects/buildpy/downloads/epub/latest/>`__ (epub) format

**Source Code**

    -   `buildpython3 <https://bitbucket.org/blakeca00/buildpython3>`__ git repository


Back to `Table Of Contents <./index.html>`__

--------------

Supported Linux Distributions
-----------------------------

**Ubuntu, Ubuntu-based Variants**

    -  |link-u1|: Ubuntu variants based on 14.04+
    -  |link-u2|: Ubuntu variants based on 16.04, 16.10+
    -  |link-u3|: Ubuntu variants based on 18.04, 18.10+
    -  |link-lm17|, |link-lm18|, |link-lm19|

**Redhat, Redhat-based Variants**

    -   |link-rh7|
    -   |link-fd|
    -   |link-cos7|
    -   |link-aml1| (2017+)
    -   |link-aml2| (2018+)


.. note::

    Older versions than listed above may be compatible, but not have not been tested


.. |link-u1| raw:: html

   <a href="http://releases.ubuntu.com/14.04" target="_blank">Ubuntu 14.04</a>

.. |link-u2| raw:: html

   <a href="http://releases.ubuntu.com/16.04" target="_blank">Ubuntu 16.04</a>

.. |link-u3| raw:: html

   <a href="http://releases.ubuntu.com/18.04" target="_blank">Ubuntu 18.04</a>

.. |link-lm17| raw:: html

   <a href="https://linuxmint.com/edition.php?id=158" target="_blank">Linux Mint 17</a>

.. |link-lm18| raw:: html

   <a href="https://www.linuxmint.com/release.php?id=31" target="_blank">Linux Mint 18</a>

.. |link-lm19| raw:: html

   <a href="https://linuxmint.com/edition.php?id=254" target="_blank">Linux Mint 19</a>

.. |link-rh7| raw:: html

   <a href="https://access.redhat.com/products/red-hat-enterprise-linux" target="_blank">Redhat 7.3+, 8</a>

.. |link-fd| raw:: html

   <a href="https://getfedora.org" target="_blank">Fedora 26+</a>

.. |link-cos7| raw:: html

   <a href="https://www.centos.org" target="_blank">CentOS 7, 8</a>

.. |link-aml1| raw:: html

   <a href="https://aws.amazon.com/amazon-linux-ami" target="_blank">Amazon Linux 1</a>

.. |link-aml2| raw:: html

   <a href="https://aws.amazon.com/amazon-linux-2" target="_blank">Amazon Linux 2</a>


Back to `Table Of Contents <./index.html>`__

--------------

Help
-----


To display the help menu:

::

    $ buildpy --help

.. image:: ../assets/help-menu.png
   :alt: help
   :scale: 100%


Back to `Table Of Contents <./index.html>`__

--------------

Author & Copyright
------------------

All works contained herein copyrighted via below author unless work is
explicitly noted by an alternate author.

.. |year| date:: %Y

-  Copyright 2017-|year| Blake Huber, All Rights Reserved.


**Sofware License**

-  Software is licensed and protected under the `GNU General Public License Agreement v3 <./license.html>`__.



Back to `Table Of Contents <./index.html>`__

--------------

Disclaimer
----------


*Code is provided "as is". No liability is assumed by either the code's
originating author nor this repo's owner for their use at AWS or any
other facility. Furthermore, running function code at AWS may incur
monetary charges; in some cases, charges may be substantial. Charges are
the sole responsibility of the account holder executing code obtained
from this library.*

Additional terms may be found in the complete `license
agreement <./LICENSE.md>`__.



--------------

`Table Of Contents <./index.html>`__

-----------------

|
