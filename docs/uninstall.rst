##########
Uninstall
##########


Debian and Ubuntu Variants
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 `buildpy <https://buildpy.readthedocs.io>`__ may be removed from your system using the debian package manager:

.. code-block:: bash

    $ sudo apt remove buildpy

--------------

Redhat, CentOS, Fedora RPM Variants
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 `buildpy <https://buildpy.readthedocs.io>`__ may be removed from your system using the yum package manager:

.. code-block:: bash

    $ sudo yum erase buildpy


.. image:: ../assets/rpm-remove-1.png
   :alt: rpm uninstall1
   :scale: 100%

Answer "y":

.. figure:: ../assets/rpm-remove-2.png
   :alt: rpm uninstall2
   :scale: 100%



--------------

`Table Of Contents <./index.html>`__

-----------------

|
