============================================
buildpy | Build Python3 for Linux
============================================


.. toctree::
   :maxdepth: 2
   :caption: README

   README
   license


.. toctree::
   :maxdepth: 2
   :caption: Installation

   installation
   upgrading
   uninstall


.. toctree::
   :maxdepth: 2
   :caption: Usage

   usecases


.. toctree::
   :maxdepth: 1
   :caption: FAQ

   FAQ


.. toctree::
   :maxdepth: 2
   :caption: Screenshots

   screenshots


.. toctree::
   :maxdepth: 2
   :caption: Developer Documentation

   modules
   builddeb
   buildrpm


.. toctree::
  :maxdepth: 2
  :caption: Versions

  releases/toctree_releases


.. toctree::
  :maxdepth: 2
  :caption: Site Map

  sitemap
