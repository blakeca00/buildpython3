.. _scnstop:

Screenshot Index
^^^^^^^^^^^^^^^^^^

**System Info** (``--info``)

    -   :ref:`osd`
    -   :ref:`pkginfo`

**Show** Functionality (``--show``):

    -   :ref:`pvs`
    -   :ref:`ospis`

**Download** (``--download``):

    -   :ref:`dpsd`
    -   :ref:`dpsn`

**Logging**

    -   :ref:`log`

**Clean** (``--clean``):

    -   :ref:`clean`


Back to `Table Of Contents <./index.html>`__

--------------

.. _osd:

Operating System Detection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Test OS detection

::

    $ buildpy  --os-detect


**Ubuntu** / **Linux Mint**

.. figure:: ../assets/os-detect.png
   :alt: os-detect


**Amazon Linux**

.. figure:: ../assets/os-detect-aml.png
   :alt: os-detect


Back to :ref:`scnstop` index

--------------

.. _pvs:

Python Binary Version Status
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Show the latest ``Python 3.7`` version available for install

::

    $ buildpy  --show  Python-3.7

.. figure:: ../assets/show3.7.png
   :alt: py3.7


Back to :ref:`scnstop` index

--------------

.. _ospis:

OS Packages Installed Status
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Show operation system package prerequisites and installed status of each

::

    $ buildpy  --show  os-packages

.. image:: ../assets/show-os-packages.png
   :alt: os-packages
   :scale: 84%


Back to :ref:`scnstop` index

--------------

.. _dpsd:

Download Python Source (Default)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ buildpy  --download        # default, download Python 3.6 binaries

.. image:: ../assets/download.png
   :alt: download
   :scale: 100%


Back to :ref:`scnstop` index

--------------

.. _dpsn:

Download Python Source, Named Version
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ buildpy  --download  3.4

.. image:: ../assets/download3.4.png
   :alt: download
   :scale: 100%


Back to :ref:`scnstop` index

--------------

.. _clean:

Remove Build Artifacts
^^^^^^^^^^^^^^^^^^^^^^^^

::

    $ buildpy --clean

.. image:: ../assets/clean.png
   :alt: clean
   :scale: 90%


Back to :ref:`scnstop` index

--------------

.. _pkginfo:

Program Information
^^^^^^^^^^^^^^^^^^^^

Detailed information regarding the local installation of `buildpy <https://buildpy.readthedocs.io>`__
program and dependencies:

::

    $ buildpy --info

.. image:: ../assets/pkg-info.png
   :alt: clean
   :scale: 100%


Back to :ref:`scnstop` index

--------------

.. _log:

Log Files
^^^^^^^^^^

 `buildpy <https://buildpy.readthedocs.io>`__ actively maintains 2 log files:


1)  **buildpy.log**

    -   Logging for all non-compile functions
    -   Main system log messages (shown below). Located at ``/var/log/buildpy.log``.

2) **console.log**:

    -   Messages during Compile & Build
    -   Populated for tracking purposes when ``--quiet`` flag set to surpress output to stdout.

::

    $ tail -n 100 /var/log/buildpy.log

.. image:: ../assets/log-sample-default.png
   :alt: logfile
   :scale: 70%


--------------

`Table Of Contents <./index.html>`__

-----------------

|
