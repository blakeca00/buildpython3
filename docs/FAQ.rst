
###################################
 Frequently Asked Questions
###################################

:ref:`general`

    - :ref:`QG1`
    - :ref:`QG2`

:ref:`permissions`

    - :ref:`QP0`
    - :ref:`QP1`

:ref:`Python Versions`

    - :ref:`QV0`
    - :ref:`QV1`
    - :ref:`QV2`

:ref:`supportdistros`

    - :ref:`QD0`
    - :ref:`QD1`

:ref:`uninstall python`

    - :ref:`QU1`

:ref:`logging`

    - :ref:`QL1`

:ref:`misc`

    - :ref:`QMISC1`

------------

.. _general:

General Questions
^^^^^^^^^^^^^^^^^^

.. _QG1:

**Q**: What is `buildpy <https://bitbucket.org/blakeca00/buildpython3>`__?  Why do I care?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*A*:  Seriously, you may not care, but if you use Linux, you probably should. `buildpy <https://bitbucket.org/blakeca00/buildpython3>`__ is only a fantastic utility or tool for anyone who needs to install a specific version of Python on a Linux hardware, virtual machine, or container.  It is also something anyone who has to remove and replace a version of Python
in machine or machine image.

.. _QG2:

*Q*: What the main use cases for `buildpy <https://bitbucket.org/blakeca00/buildpython3>`__?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*A*:  General use cases for `buildpy <https://bitbucket.org/blakeca00/buildpython3>`__ are:

1. Use buildpy to find out when new python X.Y version is available (below)

.. code:: bash

    $ buildpy --show os-packages


2. You want an easy to use, reliable means of compiling the latest version of Python after release from https://python.org.

.. code-block:: bash

    $ buildpy --profile default --operation list        # list key information


3. _Uninstall_:  You need to install a version of Python3 you compiled on your machine (not a system Python that came with your distribution)


Back to :ref:`Frequently Asked Questions` top

--------------

.. _permissions:

Required User Permissions
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _QP0:

**Q**: What permissions are required to compile and install python3 with buildpy?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: It depends on what functionality you wish to invoke.  buildpy runs the majority of operations under normal user permissions.  The exception is when compiling and installing a new version of Python3.  This is because buildpy installs a compiled version of Python3 for the entire system; thus, the ``--install`` operation requires root level permissions via sudo or assumed by running buildpy as root directly.

The following requires sudo or root permissions:

.. code-block:: bash

    $ sudo  buildpy  --install  Python3.7  [ --optimizations ]


--------------

.. _QP1:

**Q**: What happens if I run buildpy as root?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: In essence, nothing changes when you run buildpy directly as root.  You are installing a new version of Python3 for the entire system either when running buildpy as sudo or directly via root permissions.


Back to :ref:`Frequently Asked Questions` top

--------------

.. _python versions:

Python Version Support
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _QV0:

**Q**: What python3 versions can I compile and install with `buildpy <http://buildpy.readthedocs.io>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: It depends on what functionality you wish to invoke.  buildpy runs the majority of operations under normal user permissions.  The exception is when compiling and installing a new version of Python3.  This is because buildpy installs a compiled version of Python3 for the entire system; thus, the ``--install`` operation requires root level permissions via sudo or assumed by running buildpy as root directly.

The following requires sudo or root permissions:

.. code-block:: bash

    $ buildpy  --install  Python3.7  [ --optimizations ]


Back to :ref:`Frequently Asked Questions` top

--------------

.. _QV1:

**Q**: Why do I not just install python3 from my os distribution's package repository insted of using `buildpy <http://buildpy.readthedocs.io>`__?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: That is a bit of a philosophical question, so let's just stick to hard benefits that are widely accepted.  The reason you would use buildpy to compile a custom version of python3 are:

1. Compiled Python interpreters are reported to enjoy an approximate 10% execution speed improvement

2. You care about upgrading to the latest minor Python3.7 revision when it is released (example:  3.7.2 to 3.7.3).

3. You are required to run an *older* Python version (say, Python 3.4) and a modern machine for testing purposes

4. You want to enable custom functionality not enabled in the Python3 binaries installed from your OS distribution's package repository.


Back to :ref:`Frequently Asked Questions` top

--------------

.. _QV2:

**Q**: Does `buildpy <http://buildpy.readthedocs.io>`__ overwrite the old system-level Python3 version?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: No.  Let's say you have Python3.4 installed by default on a Ubuntu 14.04 system. You want to install the latest |link-qv2a|.  At the completion of the installation, |link-qv2b| will still function for os-dependent subsystems.


.. |link-qv2a| raw:: html

   <a href="https://www.python.org/downloads/release/python-373" target="_blank">Python3.4</a>


.. |link-qv2b| raw:: html

   <a href="https://www.python.org/downloads/release/python-340" target="_blank">Python3.4</a>


Back to :ref:`Frequently Asked Questions` top

--------------

.. _supportdistros:

Supported Linux Operating System Distributions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _QD0:

**Q**: If `buildpy <http://buildpy.readthedocs.io>`__ is written in `bash <https://en.wikipedia.org/wiki/Bash_(Unix_shell)>`__, I can run it on any Linux OS, right?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: Yes and No actually, or as they say in situation like this: *it depends*.

It depends upon meeting the minimum requirements outlined in the `depenencies <file:///home/blake/git/linux/buildpython3/docs/_build/html/installation.html>`__ section.  ``Bash 4.4+`` is required as well as a few other critical os packages such as |link-qd0a|, |link-qd0b|, and |link-qd0c|.


.. |link-qd0a| raw:: html

    <a href="https://linux.die.net/man/1/sed" target="_blank">sed</a>

.. |link-qd0b| raw:: html

    <a href="http://linuxcommand.org/lc3_adv_awk.php" target="_blank">awk</a>

.. |link-qd0c| raw:: html

   <a href="https://www.gnu.org/software/bc/manual/html_mono/bc.html" target="_blank">bc</a>


Back to :ref:`Frequently Asked Questions` top

--------------

.. _QD1:

**Q**: What Redhat-based distributions does `buildpy <http://buildpy.readthedocs.io>`__ install via an RPM package?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: See the list |link-qd1a|. `buildpy <http://buildpy.readthedocs.io>`__ is also compatible with Amazon Linux 2 when run on EC2 virtual machines.


.. |link-qd1a| raw:: html

   <a href="https://buildpy.readthedocs.io/en/rtd/README.html#supported-linux-distributions" target="_blank">here</a>




Back to :ref:`Frequently Asked Questions` top

--------------

.. _uninstall python:

Uninstall Python3
^^^^^^^^^^^^^^^^^^

.. _QU1:

**Q**: What Python3 versions can I safely remove from my system with `buildpy <http://buildpy.readthedocs.io>`__?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: As a built-in safeguard, `buildpy <http://buildpy.readthedocs.io>`__ will only remove compiled versions from your system.  This is any python installed in ``/usr/local/``.  This is location of any python versions compiled from source.

Most if not all system-wide Python binaries install from your Linux distribution's official package repository reside in ``/usr/bin``; thus, `buildpy <http://buildpy.readthedocs.io>`__ avoids this location and will *not remove* python binaries from this location as a precaution.

To initiate the uninstall operation, type the following:

.. code:: bash

    $ sudo buildpy --uninstall Python3.6


Back to :ref:`Frequently Asked Questions` top

--------------

.. _logging:

Logging FAQ
^^^^^^^^^^^

.. _QL1:

**Q**: Which log files does `buildpy <http://buildpy.readthedocs.io>`__ write to?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: `buildpy <http://buildpy.readthedocs.io>`__ produces the following log files:

    -   ``/var/log/buildpy.log`` contains summary messages produced during normal operations
    -   ``/var/log/console.log`` contains all console messages produced only during compile and install operations


Back to :ref:`Frequently Asked Questions` top

--------------

.. _misc:

Miscellaneous Questions
^^^^^^^^^^^^^^^^^^^^^^^^

.. _QMISC1:

**Q**: Since `buildpy <http://buildpy.readthedocs.io>`__ requires an Internet connection to download python binaries, does it send any information to you or anyone else?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**A**: None whatsoever.  `buildpy <http://buildpy.readthedocs.io>`__ can actually function fine without any Internet connection if the python binaries are downloaded offline and placed in ``/tmp`` so that they used as the source during the install.


Back to :ref:`Frequently Asked Questions` top

--------------

`Table Of Contents <./index.html>`__

-----------------

|
