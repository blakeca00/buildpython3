===============================
 v1.8.9 \| Release Notes
===============================


**Release date**:  December 22, 2020

Maintenance Release

--------------


Maintenance, v1.8.9
-------------------------


Added additional Operating System package dependencies to satisfy **uuid** requirements during Python binary compilation for the following OS variants:

    * |link-ubuntu| (All supported versions).

    * |link-linuxmint| (All supported versions).

    * |link-redhat| (All supported versions).

    * |link-amzn| (Version 2).

    * |link-fc| (All supported versions).


.. |link-ubuntu| raw:: html

   <a href="https://ubuntu.com" target="_blank"><b>Ubuntu</b></a>

.. |link-linuxmint| raw:: html

   <a href="https://linuxmint.com" target="_blank"><b>Linux Mint</b></a>

.. |link-redhat| raw:: html

   <a href="https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux" target="_blank">Redhat Enterprise Linux</a>

.. |link-amzn| raw:: html

   <a href="https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-ami-basics.html" target="_blank">Amazon Linux</a>

.. |link-fc| raw:: html

   <a href="https://getfedora.org/en/workstation/" target="_blank">Fedora Workstation</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
