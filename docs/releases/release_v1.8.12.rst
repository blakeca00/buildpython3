===============================
 v1.8.12 \| Release Notes
===============================


**Release date**:  May 21, 2021

Maintenance Release

--------------


Maintenance, v1.8.12
-------------------------


Added support for 2-digit Python minor releases with the release of |link-python310|.  Previous versions contained a bug which prevented **buildpy** from recognizing Python binary releases higher than the 9 minor version (i.e. higher than 3.9 for example).


.. |link-python310| raw:: html

   <a href="https://docs.python.org/3.10" target="_blank">Python 3.10</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
