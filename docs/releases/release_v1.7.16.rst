===============================
 v1.7.16 \| Release Notes
===============================


**Release date**:  August 3, 2019

Functionality & Maintenance Release

--------------


Feature Updates, v1.7.16
------------------------

    * Complete rewrite to function `package_info` to enable more info on single screen presentation.
    * Bug Fix, Various


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
