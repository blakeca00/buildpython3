===============================
 v1.7.18 \| Release Notes
===============================


**Release date**:  September 26, 2019

Feature Release

--------------


Feature Updates, v1.7.18
---------------------------

    * As of release 1.7.18, |link-1.7.18a| can now compile and install Python release candidate, Beta, and Alpha code releases.  This change was made to enable testing with Python 3.8+ release candidates in the future.


.. |link-1.7.18a| raw:: html

   <a href="https://bitbucket.org/blakeca00/buildpython3" target="_blank">buildpy</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
