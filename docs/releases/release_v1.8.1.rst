===============================
 v1.8.1 \| Release Notes
===============================


**Release date**:  January 10, 2020

Feature Release

--------------


Feature Updates, v1.8.1
-------------------------

    **Install a specific Python binary set**.  User can select an exact Python binary set to install instead of the latest release.  For example, previously, if one wanted to install Python 3.6, and the latest minor revision available was `3.6.8`, only `3.6.8` would be compiled and installed.  User can now provide the full revision number when installing and **buildpy** will compile and install that exact revision set as follows:

    ::

        $ sudo buildpy --install Python-3.6.5

    Alternatively, the following syntax is equivalent:

    ::

        $ sudo buildpy --install 3.6.5


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
