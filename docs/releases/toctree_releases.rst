.. _latest:

Current Release
^^^^^^^^^^^^^^^


.. toctree::
   :maxdepth: 1

   release_v1.8.12


Release History
^^^^^^^^^^^^^^^

   +-----------------------------+------------------+
   |  buildpy Release Version    |   Date           |
   +=============================+==================+
   | |link-r1.8.12|              | May 21, 2021     |
   +-----------------------------+------------------+
   | |link-r1.8.11|              | Dec 27, 2020     |
   +-----------------------------+------------------+
   | |link-r1.8.10|              | Dec 26, 2020     |
   +-----------------------------+------------------+
   | |link-r1.8.9|               | Dec 22, 2020     |
   +-----------------------------+------------------+
   | |link-r1.8.8|               | July 24, 2020    |
   +-----------------------------+------------------+
   | |link-r1.8.5|               | January 28, 2020 |
   +-----------------------------+------------------+
   | |link-r1.8.3|               | January 12, 2020 |
   +-----------------------------+------------------+
   | |link-r1.8.1|               | January 10, 2020 |
   +-----------------------------+------------------+
   | |link-r1.7.20|              | October 29, 2019 |
   +-----------------------------+------------------+
   | |link-r1.7.19|              | October 26, 2019 |
   +-----------------------------+------------------+
   | |link-r1.7.18|              | Sept 26, 2019    |
   +-----------------------------+------------------+
   | |link-r1.7.17|              | Sept 24, 2019    |
   +-----------------------------+------------------+
   | |link-r1.7.16|              | August 3, 2019   |
   +-----------------------------+------------------+
   | |link-r1.7.15|              | April 2, 2019    |
   +-----------------------------+------------------+
   | Release **1.7.14**          | April 1, 2019    |
   +-----------------------------+------------------+
   | Release **1.1.13**          | March 31, 2019   |
   +-----------------------------+------------------+
   | Release **1.7.8**           | December 8, 2019 |
   +-----------------------------+------------------+
   | Release **1.1.0**           | January 23, 2019 |
   +-----------------------------+------------------+
   | Release **1.0.1**           | December 30, 2017|
   +-----------------------------+------------------+



Release Note Index
^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   release_v1.8.12
   release_v1.8.11
   release_v1.8.10
   release_v1.8.9
   release_v1.8.8
   release_v1.8.5
   release_v1.8.3
   release_v1.8.1
   release_v1.7.20
   release_v1.7.19
   release_v1.7.18
   release_v1.7.17
   release_v1.7.16
   release_v1.7.15


.. |link-r1.8.12| raw:: html

 <a href="../releases/release_v1.8.12.html">Release v1.8.12</a>

.. |link-r1.8.11| raw:: html

 <a href="../releases/release_v1.8.11.html">Release v1.8.11</a>

.. |link-r1.8.10| raw:: html

 <a href="../releases/release_v1.8.10.html">Release v1.8.10</a>

.. |link-r1.8.9| raw:: html

 <a href="../releases/release_v1.8.9.html">Release v1.8.9</a>

.. |link-r1.8.8| raw:: html

 <a href="../releases/release_v1.8.8.html">Release v1.8.8</a>

.. |link-r1.8.5| raw:: html

 <a href="../releases/release_v1.8.5.html">Release v1.8.5</a>

.. |link-r1.8.3| raw:: html

 <a href="../releases/release_v1.8.3.html">Release v1.8.3</a>

.. |link-r1.8.1| raw:: html

 <a href="../releases/release_v1.8.1.html">Release v1.8.1</a>

.. |link-r1.7.20| raw:: html

 <a href="../releases/release_v1.7.18.html">Release v1.7.20</a>

.. |link-r1.7.19| raw:: html

 <a href="../releases/release_v1.7.18.html">Release v1.7.19</a>

.. |link-r1.7.18| raw:: html

 <a href="../releases/release_v1.7.18.html">Release v1.7.18</a>

.. |link-r1.7.17| raw:: html

 <a href="../releases/release_v1.7.17.html">Release v1.7.17</a>

.. |link-r1.7.16| raw:: html

 <a href="../releases/release_v1.7.16.html">Release v1.7.16</a>

.. |link-r1.7.15| raw:: html

 <a href="../releases/release_v1.7.15.html">Release v1.7.15</a>



--------------

`Table Of Contents <../index.html>`__

--------------

|
