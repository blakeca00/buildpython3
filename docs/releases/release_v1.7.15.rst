===============================
 v1.7.15 \| Release Notes
===============================


**Release date**:  April 2, 2019

Documentation Release

--------------

Documentation, v1.7.15
-----------------------

    * Multiple updates to `Summary <../README.html>`__ and `Releases <./toctree_releases.html>`__ sections.

Maintenance, v1.1.3
--------------------

    * Fixed uninstall operation deleting out-of-band references to python version for removal
    * Bug Fix, Various


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
