===============================
 v1.8.3 \| Release Notes
===============================


**Release date**:  January 12, 2020

Feature Release

--------------


Feature Updates, v1.8.3
-------------------------

    * **Support for CentOS 8 Linux**.  Compatibility tested for both 7 and 8 versions of CentOS.

    * **Support for Python 3.9**.  Python 3.9 now included as an optional major version choice for installation.

--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
