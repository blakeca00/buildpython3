===============================
 v1.7.20 \| Release Notes
===============================


**Release date**:  October 29, 2019

Feature Release

--------------


Feature Updates, v1.7.20
-------------------------

    * User can now choose whether to install release candidate python build sets if downloaded from python.org.

    * Issue #58 Resolved:  buildpy now checks system-installed python major version before installing. Change prevents installing a compiled python binary executable which is already installed on the system.


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
