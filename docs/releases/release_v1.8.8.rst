===============================
 v1.8.8 \| Release Notes
===============================


**Release date**:  July 24, 2020

Maintenance Release

--------------


Maintenance, v1.8.8
-------------------------

    * |link-ubuntu| **20.04 Support**.  Added support for newly released |link-U20| (LTS and non-LTS minor releases).

    * |link-linuxmint| **20 Support**.  Added support for newly released variants such as |link-LM20|.


.. |link-ubuntu| raw:: html

 <a href="https://ubuntu.com" target="_blank"><b>Ubuntu</b></a>

.. |link-linuxmint| raw:: html

 <a href="https://linuxmint.com" target="_blank"><b>Linux Mint</b></a>

.. |link-U20| raw:: html

 <a href="https://releases.ubuntu.com/20.04" target="_blank">Ubuntu 20.04</a>

.. |link-LM20| raw:: html

 <a href="https://linuxmint.com/download.php" target="_blank">Linux Mint 20</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
