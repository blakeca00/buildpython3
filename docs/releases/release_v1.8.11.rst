===============================
 v1.8.11 \| Release Notes
===============================


**Release date**:  December 27, 2020

Maintenance Release

--------------


Maintenance, v1.8.11
-------------------------


1. Added additional Operating System package dependencies to satisfy |link-tkinter| dependencies during binary compilation.

    Dependencies updated in this release for the following Linux OS variants:

    * |link-redhat| (All supported versions).

    * |link-amzn| (Version 2).

    * |link-fc| (All supported versions).

2. Added new function to create global ``python3`` symlink if it does not already exist on the system.


.. |link-tkinter| raw:: html

   <a href="https://wiki.python.org/moin/TkInter" target="_blank">tkinter</a>

.. |link-redhat| raw:: html

   <a href="https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux" target="_blank">Redhat Enterprise Linux</a>

.. |link-amzn| raw:: html

   <a href="https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-ami-basics.html" target="_blank">Amazon Linux</a>

.. |link-fc| raw:: html

   <a href="https://getfedora.org/en/workstation/" target="_blank">Fedora Workstation</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
