===============================
 v1.8.10 \| Release Notes
===============================


**Release date**:  December 26, 2020

Maintenance Release

--------------


Maintenance, v1.8.10
-------------------------


Added additional Operating System package dependencies to satisfy |link-lzma| and |link-xz| compression algorithm dependencies required during Python binary compilation.

* Debian-based Linux OS Packages added:
    * liblzma-dev | XZ-format compression library - development files
    * liblz-dev	| data compressor based on the LZMA algorithm (development)
    * lzma-dev | Compression and decompression in the LZMA format - development files
    * libqdbm-dev | QDBM Database Libraries [development]
    * libgdbm-dev | GNU dbm database routines (development files)

* Redhat-based Linux OS Packages added:
    * lzma-sdk | SDK for lzma compression
    * lzma-sdk-devel | Development libraries and headers for lzma-sdk
    * xz-devel | Devel libraries & headers for liblzma
    * gdbm-devel | Development libraries and header files for the GNU Database system


Dependencies updated in this release for the following Linux OS variants:

    * |link-ubuntu| (All supported versions).

    * |link-linuxmint| (All supported versions).

    * |link-redhat| (All supported versions).

    * |link-amzn| (Version 2).

    * |link-fc| (All supported versions).


.. |link-xz| raw:: html

   <a href="https://en.wikipedia.org/wiki/XZ_Utils" target="_blank">XZ</a>

.. |link-lzma| raw:: html

   <a href="https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Markov_chain_algorithm" target="_blank">Lempel–Ziv–Markov (lzma)</a>

.. |link-ubuntu| raw:: html

   <a href="https://ubuntu.com" target="_blank"><b>Ubuntu</b></a>

.. |link-linuxmint| raw:: html

   <a href="https://linuxmint.com" target="_blank"><b>Linux Mint</b></a>

.. |link-redhat| raw:: html

   <a href="https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux" target="_blank">Redhat Enterprise Linux</a>

.. |link-amzn| raw:: html

   <a href="https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-ami-basics.html" target="_blank">Amazon Linux</a>

.. |link-fc| raw:: html

   <a href="https://getfedora.org/en/workstation/" target="_blank">Fedora Workstation</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
