===============================
 v1.7.19 \| Release Notes
===============================


**Release date**:  October 26, 2019

Maintenance Release

--------------


Maintenance Updates, v1.7.19
-------------------------------

    * As of release 1.7.18, |link-1.7.19a| can now compile and install Python release candidate, Beta, and Alpha code releases.  Fix remaining issues with code released in version 1.7.18.


.. |link-1.7.19a| raw:: html

   <a href="https://bitbucket.org/blakeca00/buildpython3" target="_blank">buildpy</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
