===============================
 v1.8.5 \| Release Notes
===============================


**Release date**:  January 28, 2020

Maintenance Release

--------------


Maintenance, v1.8.5
-------------------------

    * **Python Version Reconciliation**.  Fixed Issue #25/#36:  Failure to recognize faulty input for Python version


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
