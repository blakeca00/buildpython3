===============================
 v1.7.17 \| Release Notes
===============================


**Release date**:  September 24, 2019

Maintenance Release

--------------


Maintenance, v1.7.17
--------------------

    * Release to fix various compatibility issues with modern |link-1.7.17a| releases (> version 25)


.. |link-1.7.17a| raw:: html

   <a href="https://fedoraproject.org" target="_blank">Fedora Linux</a>


--------------

( `Back to Releases <./toctree_releases.html>`__ )

--------------

|
