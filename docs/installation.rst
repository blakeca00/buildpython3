

Dependencies
------------


-  *Root Privileges*. Ability to assume user with root privileges or access such privileges via **sudo**

-  **Python3-support via Operating System Packages**. Dependencies on various operating system packages available from official Linux distribution's repository. You can view os package dependencies with the following command once **buildpy** is installed:

-  **Bash 4.1 or higher**. No preexisting python implementation is required. **buildpy** is written in `bash <https://www.gnu.org/software/bash/>`__.


Back to `Table Of Contents <./index.html>`__

--------------

Installation
------------


Debian, Ubuntu, Linux Mint, Ubuntu Variants
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Method 1**: Install the **debian-tools** repository.

The easiest way to install **buildpy** is via the Debian-tools repository:


1. Download the public key:

   ::

       $ wget -qO - http://awscloud.center/keys/public.key | sudo apt-key add -


2. Install the repository:

   ::

       $ string='deb [arch=amd64] http://deb.awscloud.center <distribution> main' ; \
         sudo echo $string > /etc/apt/sources.list.d/debian-tools.list


   Where ``<distribution>`` is one of the following:

    -  ``trusty``: Ubuntu 14.04, Ubuntu 14.04 based Linux distributions
    -  ``xenial``: Ubuntu 16.04, 16.04 based Linux distributions
    -  ``bionic``: Ubuntu 18.04, 18.04 based Linux distributions


3. Verify package repository installation

   ::

       $ apt list buildpy -a

.. figure:: ../assets/repo-contents.png
   :alt: repository-contents


4. Update and install the package:

   ::

       $ sudo apt update  &&  sudo apt install buildpy


5. Verify Installation. To verify a Debian (.deb) package installation:

   ::

       $ apt show buildpy

.. figure:: ../assets/apt-show.png
   :alt: apt


Back to `Table Of Contents <./index.html>`__

--------------

**Method 2**: Install directly via ``.deb`` package.

| If you do not want to install an additional repository on your local
  machine, you may instead download
| and install the .deb package using the ``apt`` or ``apt-get`` package
  mangers.

1. **Download**: the .deb installation package (v1.6.3 shown):

   ::

       $ wget https://bitbucket.org/blakeca00/buildpython3/downloads/buildpy-1.6.3_amd64.deb

2. **Install** via apt package manager:

   ::

       $ sudo apt install ./buildpy-1.6.3_amd64.deb

**Note**:

-  Installation of the .deb package will also install the
   **debian-tools** repository on your local machine.
   If you do not want this, please comment out the single line in
   ``/etc/apt/sources.list.d/debian-tools.list``.

Back to `Table Of Contents <./index.html>`__

--------------

Redhat, CentOS, Fedora RPM-based Distributions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The easiest way to install **buildpy** on redhat-based Linux
distributions is via the developer-tools package repository:

(**Fedora** Users: use ``dnf`` in place of ``yum`` below)

1. Download and install the repo definition file

   ::

       $ sudo yum install wget

   ::

       $ wget https://bitbucket.org/blakeca00/buildpython3/downloads/developer-tools.repo

   ::

       $ sudo mv developer-tools.repo /etc/yum.repos.d/  &&  sudo chown 0:0 developer-tools.repo

2. Update local repository cache

   ::

       $ sudo yum update -y

3. Install **buildpy** os package

   ::

       $ sudo yum install buildpy

   .. figure:: ../assets/rpm-install-1.png
      :alt: rpm install

      rpm install

   Answer "y":

   .. figure:: ../assets/rpm-install-2.png
      :alt: rpm install

      rpm install

4. Verify Installation

   ::

       $ yum info buildpy

   .. figure:: ../assets/verify-rpm-install.png
      :alt: verify-rpm

--------------

`Table Of Contents <./index.html>`__

-----------------

|
