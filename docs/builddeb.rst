
.. _dpc:

Debian Package Creation
^^^^^^^^^^^^^^^^^^^^^^^^

- :ref:`builddeb1`
- :ref:`builddeb2`
- :ref:`builddeb3`


---------------

.. _builddeb1:

Debian Package Creation Start
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    $ make builddeb

.. image:: ../assets/builddeb/deb-install1.png
   :alt: rpm package creation
   :scale: 65%


Back to :ref:`dpc` Index

--------------

.. _builddeb2:

Debian Package Creation Build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. image:: ../assets/builddeb/deb-install2.png
   :alt: rpm package creation
   :scale: 65%


Back to :ref:`dpc` Index

--------------

.. _builddeb3:

Debian Package Final Contents
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. image:: ../assets/builddeb/deb-install3.png
   :alt: rpm package creation
   :scale: 90%


Back to :ref:`dpc` Index

--------------

Back to `Table Of Contents <./index.html>`__

-----------------

|
