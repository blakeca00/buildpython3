.. _uc:

Use Cases
^^^^^^^^^^

- :ref:`verify`
- :ref:`installbasic`
- :ref:`installopt`
- :ref:`specificversion`
- :ref:`unattended`
- :ref:`uninstallpython`


.. important::

    | Root privileges via sudo *required*; otherwise execute directly as root user


---------------

.. _verify:

Verify Operating System Dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    $ buildpy --show os-packages

.. image:: ../assets/show-os-packages.png
   :alt: os-packages
   :scale: 78%


Back to :ref:`uc` top

--------------

.. _installbasic:

Compile Python 3 - Basic Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To compile and install the latest Python-3.6 binaries (version 3.6.7 at the time of this post), use the following command:

::

    $ sudo buildpy --install Python-3.6

Or as root directly:

::

    $ sudo su -

::

    root@dev:~# buildpy --install 3.6


Back to :ref:`uc` top

--------------

.. _installopt:

Compile Python 3 - Advanced Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Python 3 Installation Compiled with Optimizations**

**buildpy** can compile Python 3 binaries `with optimizations <https://en.wikipedia.org/wiki/Profile-guided_optimization>`__.  Although this will lengthen the compile time considerably (30 minutes - 1 hour depending upon system resources), the speed of the Python interpreter code execution will increase by 10% - 20%.

::

    $ sudo buildpy --install Python-3.6 --optimizations


**Compilation Parallelism**

When compiling with optimizations, you may wish to increase the number of parallel compilation processes to decrease the duration of the compile time.  With **buildpy**, this is configurable via the ``--parallel-processes`` option.  Up to 9 cpus can be utilized in parallel during compile time.

::

    $ sudo buildpy --install Python-3.6 --optimizations --parallel-processes 8


The above options will compile Python 3.6 binaries with optimizations utilising 8 cpu's in parallel.


.. admonition:: Note

    *4 cpu's is the default parallelism*; i.e. 4 cpus are used in parallel during the compile.


Back to :ref:`uc` top

--------------

.. _specificversion:

Compile Python 3 - Specific Version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you wish to compile and install a specific Python 3 binary, meaning a specific major and minor Python 3 version, simply add the full version label instead of just providing the Python major revision number:

::

    $ sudo buildpy --install Python-3.7.6


Alternatively, for all installation types listed in this guide, the *Python-* prefix may be omitted:

::

    $ sudo buildpy --install 3.7.6


The two commands above are equivalent and will compile and install Python 3 version 3.6.9.


Back to :ref:`uc` top

--------------

.. _unattended:

Unattended Install
~~~~~~~~~~~~~~~~~~~

If run via unattended script, use ``--quiet`` to suppress stdout messages:

::

    $ sudo buildpy --install 3.6 --quiet


A running log is created in all execution states to ``/var/log/buildpy.log``. See an `example log file <https://buildpy.readthedocs.io/en/latest/screenshots.html#log-files>`__.


Back to :ref:`uc` top

--------------

.. _uninstallpython:

Uninstalling Python 3 Versions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

At any time you may choose to uninstall a system-wide, compiled version of Python 3 using **buildpy**.  The following command example uninstalls Python 3.6:

::

    $ sudo buildpy --uninstall 3.6 --purge


If a system-wide Python 3 version pre-existed prior to installing a compiled Python 3 version, the `python3` symlink will be redirected to the previous Python 3 version once the uninstall operation completes.


.. important::

    | For stability, *only compiled Python 3 versions can be uninstalled* with **buildpy**.  Native, system-wide Python versions installed from the operating system package repositories must uninstalled using the OS package manager application.


Back to :ref:`uc` top

--------------

Back to `Table Of Contents <./index.html>`__

-----------------

|
