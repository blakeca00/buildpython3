.. _upgradetop:

Upgrading
----------


To discover and apply newly-released versions of BUILDPY, follow the
steps below for your specific distribution.

Debian, Ubuntu Variants
~~~~~~~~~~~~~~~~~~~~~~~~~~

**(1)** Find out if an upgraded version of  `buildpy <https://buildpy.readthedocs.io>`__ is available:

::

    $ sudo apt update && sudo apt list --upgradeable

.. image:: ../assets/apt-upgrade-available.png
   :alt: repository-contents


Alternate:

::

    $ apt list buildpy -a

.. image:: ../assets/repo-contents-upgradeable.png
   :alt: repository-contents


**(2)** Install available upgrades:

::

    $ sudo apt upgrade

**(3)** Verify upgrade:

::

    $ sudo apt list buildpy -a

.. image:: ../assets/repo-contents-installed.png
   :alt: repository-contents


Back to `Table Of Contents <./index.html>`__

--------------

Upgrading Redhat-based Distributions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**(1)** Find out if an upgraded version of  `buildpy <https://buildpy.readthedocs.io>`__ is available

::

    $ sudo yum info buildpy

.. image:: ../assets/repo-yum-info.png
   :alt: repository-contents
   :scale: 90%


**(2)** Install available upgrades:

::

    $ sudo yum update -y

.. image:: ../assets/repo-yum-update.png
   :alt: repository-contents
   :scale: 90%


--------------

`Table Of Contents <./index.html>`__

-----------------

|
