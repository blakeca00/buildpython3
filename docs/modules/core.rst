.. _coretop:

colors.sh
~~~~~~~~~~

Back to :ref:`Bash Module Index`

.. literalinclude:: ../../core/colors.sh
   :linenos:
   :language: bash


Back to :ref:`Bash Module Index`

----------------------------

.. _exitcodes:

exitcodes.sh
~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../core/exitcodes.sh
   :linenos:
   :language: bash


Back to :ref:`Bash Module Index`

----------------------------

.. _version:

version\.py
~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../core/version.py
   :linenos:
   :language: python



--------------

Back to :ref:`Bash Module Index`

-----------------

|
