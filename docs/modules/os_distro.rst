

os_distro.sh
~~~~~~~~~~~~~~~~~

Back to :ref:`Bash Module Index`

.. literalinclude:: ../../core/os_distro.sh
   :linenos:
   :language: bash


Back to :ref:`Bash Module Index`

--------------

`Table Of Contents <../index.html>`__

-----------------

|
