* * *
# Uninstaller | Python3
* * *

## Caution

Although this has been tested on supported distributions, when invoked, you are deleting the system
Python3 binaries.  If more than 1 python3 binary is installed, all may be deleted.

Use at your own risk.

* * *

## Details


### reported rm locations:
sudo rm -rf /usr/local/bin/python3.6 /usr/local/bin/pydoc3 /usr/local/lib/python3.1 /usr/local/include/python3.6      /usr/local/lib/pkgconfig/python3.pc /usr/local/lib/libpython3.1.a


https://stackoverflow.com/questions/39261624/uninstall-python-3-5-2-from-redhat-linux

```bash

declare -a ARRAY
ARRAY=$(whereis python3 | awk -F ':' '{print $2}')

for path in ${ARRAY[@]}; do
    echo "deleting $path";
    sudo rm -fr $path
    sleep 3
done

```

CODE ABOVE TESTED ON:
* centos
